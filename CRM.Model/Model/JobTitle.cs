﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Model.Model
{
    public class JobTitle
    {
        public JobTitle()
        {
            this.Customers = new List<Customer>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]   

        public long Id { get; set; }

        [Required(ErrorMessage = "Job Title is required")]
        public string Title { get; set; }

        public string Description { get; set; }

        public virtual IList<Customer> Customers { get; set; }

    }
}
