﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CRM.DataAccessLayer.Interface
{
    public interface IGenericRepository<T>  where T: class
    {

        IEnumerable<T> GetAll();

        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);

        T SingleOrDefault(Expression<Func<T, bool>> predicate);

        T GetById(long id);

        T GetById(int id);

        void Add(T entity);

        void Delete(T entity);

        void Update(T entity);

    }
}
