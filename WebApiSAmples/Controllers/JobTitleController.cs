﻿using CRM.DataAccessLayer.Interface;
using CRM.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApiSAmples.ViewModels;

namespace WebApiSAmples.Controllers
{
    public class JobTitleController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public JobTitleController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region Basic CRUD


        [HttpGet]
        public ActionResult CreateNewJobTitle ()
        {
            JobTitleDetail jobTitleDetail = new JobTitleDetail();

            return View(jobTitleDetail);

        }

        [HttpPost]
        public ActionResult CreateNewJobTitle(JobTitleDetail jobTitle)
        {

            if (ModelState.IsValid)
            {
                this._unitOfWork.JobTitleRepository.Add(new JobTitle { });
                this._unitOfWork.Commit();
            }

            return View();
        }



        #endregion


    }
}