﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApiSAmples.ViewModels
{
    public class JobTitleDetail
    {
        [HiddenInput(DisplayValue = false)]
        public long Id { get; set; }

        [Required(ErrorMessage = "Job-title is required")]
        [StringLength(50, ErrorMessage = "Job-title must have at least 3 to 50 characters.", MinimumLength = 3)]
        public string Title { get; set; }


        [StringLength(100, ErrorMessage = "Description must have at least 10 to 100 characters", MinimumLength = 10)]
        public string Description { get; set; }

    }
}