﻿using CRM.DataAccessLayer.Repositories.Generic;
using CRM.Model.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.BusinessLayer.Repositories
{
    public class JobTitleRepository : GenericRepository<JobTitle>
    {
        public JobTitleRepository(DbSet<JobTitle> dbSet) : base(dbSet) { }
    }
}
