﻿using CRM.DataAccessLayer.Interface;
using CRM.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using CRM.DataAccessLayer.Repositories.Generic;
using System.Data.Entity;

namespace CRM.BusinessLayer.Repositories
{


    public class CustomerRepository : GenericRepository<Customer>
    {

        public CustomerRepository(DbSet<Customer> dbSet) : base(dbSet) { }
    }
}
