namespace CRM.BusinessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CRM1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ClientId = c.String(),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        MiddleName = c.String(nullable: false),
                        MiddleInitial = c.String(nullable: false),
                        FullName = c.String(nullable: false),
                        JobId = c.Long(nullable: false),
                        JobTitle_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobTitles", t => t.JobTitle_Id, cascadeDelete: true)
                .Index(t => t.JobTitle_Id);
            
            CreateTable(
                "dbo.JobTitles",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Customers", "JobTitle_Id", "dbo.JobTitles");
            DropIndex("dbo.Customers", new[] { "JobTitle_Id" });
            DropTable("dbo.JobTitles");
            DropTable("dbo.Customers");
        }
    }
}
