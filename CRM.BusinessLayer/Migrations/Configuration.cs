namespace CRM.BusinessLayer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CRM.BusinessLayer.EFUnitOfWork>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CRM.BusinessLayer.EFUnitOfWork context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.JobTitleRepository.Add(new Model.Model.JobTitle {  Title = "Associate Software Engineer",   Description = ""    });

            context.JobTitleRepository.Add(new Model.Model.JobTitle {  Title = "Software Engineer",             Description = ""    });

            context.JobTitleRepository.Add(new Model.Model.JobTitle {  Title = "Senior Software Engineer",      Description = ""    });

            context.JobTitleRepository.Add(new Model.Model.JobTitle {  Title = "System Analyst",                Description = ""    });

            context.JobTitleRepository.Add(new Model.Model.JobTitle {  Title = "Business Analyst",              Description = ""    });

            context.SaveChanges();
        }
    }
}
