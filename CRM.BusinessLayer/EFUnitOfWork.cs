﻿using CRM.DataAccessLayer.Interface;

using System.Data.Entity;

using CRM.Model.Model;
using CRM.DataAccessLayer.Repositories.Generic;
using CRM.BusinessLayer.Repositories;

namespace CRM.BusinessLayer
{
    public class EFUnitOfWork : DbContext, IUnitOfWork
    {
        private readonly GenericRepository<Customer> _customerRepository;
        private readonly GenericRepository<JobTitle> _jobTitleRepository;


        public DbSet<Customer> Customers { get; set; }
        public DbSet<JobTitle> JobTitles { get; set; }

        public EFUnitOfWork() : base("CRMConnectionStrings")
        {

            this._customerRepository = new CustomerRepository(Customers);

            this._jobTitleRepository = new JobTitleRepository(JobTitles);

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JobTitle>()
                .HasKey(j => j.Id)
                .HasMany(j => j.Customers);
               

            modelBuilder.Entity<Customer>()
                .HasKey(c => c.Id)
                .HasRequired(c => c.JobTitle);


        }

        public IGenericRepository<Customer> CustomerRepository
        {
            get
            {
                return this._customerRepository;
            }
        }

        public IGenericRepository<JobTitle> JobTitleRepository
        {
            get
            {
                return this._jobTitleRepository;
            }
        }

        public void Commit()
        {
            this.SaveChanges();
        }
    }
}
